# Frontend Test

This project was build using create-react-app and json-server.

In order to run the project:

* `npm i`
* `npm run start` - start project
* `npm run api` - start api
* `npm run dep-graph` - generate dependency graph

It might seem like I have overcomplicated things but that's far from true. I've written a custom redux like implementation that utilizes the React hooks api. The reason that I have not just installed Redux is because I wanted to show that I know what is going on rather than just being able to use a package.
(redux devtools work too)

I've used two different approaches for testing and currently in my own projects I am messing around with `React-testing-library` (created by Kent C Dodds).

Should there be any questions I'd be available on sebastiaan@vanarkens.nl
