import React, { Component } from "react";
import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";

// State
import Provider from "./store/Provider";
import reducers from './reducers';
import SearchBox from "./components/SearchBox";

const Style = createGlobalStyle`
    ${reset}
    * {
        outline: none;
        box-sizing: border-box;
    }
    body {
        font: normal 1em Sans-serif;
    }
    :root {
        --border-color: #979797;
        --font-color: #4A4A4A;
        --button-color: #B4B4B4;
        --button-color-disabled: #fff;
        --select-background: #E6E6E6;
    }
`;

class App extends Component {
    render() {
        return (
            <Provider reducer={reducers}>
                <Style />
                <SearchBox />
            </Provider>
        );
    }
}

export default App;
