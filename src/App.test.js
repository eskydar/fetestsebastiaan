import React from "react";
import TestRenderer from "react-test-renderer";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "./App";
import Select from "./components/Select";

const act = TestRenderer.act;
configure({ adapter: new Adapter() });

describe("App test", () => {
    it("renders without crashing", () => {
        act(() => {
            TestRenderer.create(<App />);
        });
    });
});

describe("Select test", () => {
    let SelectComponent;
    let props;

    // store mounted component that is available everywhere within the describe function
    const selectComponent = () => {
        if (!SelectComponent) {
            SelectComponent = mount(<Select {...props} />);
        }
        return SelectComponent;
    };
    // reset props and mounted component before every test.
    // Otherwise, state from one test would leak into another.
    beforeEach(() => {
        props = { options: [], placeholder: "Test Select" };
        SelectComponent = undefined;
    });

    it("should be disabled if there are no values", () => {
        const wrapper = selectComponent();
        expect(wrapper.find("select").props().disabled).toBe(true);
    });

    it("should be disabled if there are no values", () => {
        props.options = [{ name: "test", value: 1 }];
        const wrapper = selectComponent();
        expect(wrapper.find("select").props().disabled).toBe(false);
    });
});
