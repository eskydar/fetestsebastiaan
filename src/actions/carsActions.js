import axios from "axios";
import { clearField } from "./formActions";

export const requestBrandsLoading = "requestBrandsLoading";
export const requestBrandsError = "requestBrandsError";
export const requestBrandsSuccess = "requestBrandsSuccess";
export const requestModelsLoading = "requestModelsLoading";
export const requestModelsError = "requestModelsError";
export const requestModelsSuccess = "requestModdelsSuccess";
export const clearModels = "clearModels";

const baseUrl = "http://localhost:3001/";

export const resetModels = () => ({ dispatch }) => {
    dispatch({ type: clearModels });
    dispatch({ type: clearField, payload: "model" });
};

export const requestBrands = () => async ({ dispatch }) => {
    dispatch({ type: requestBrandsLoading });
    console.log('asd');
    try {
        const request = await axios.get(`${baseUrl}brands`);
        const response = await request.data;
        dispatch({ type: requestBrandsSuccess, payload: response });
    } catch (e) {
        dispatch({ type: requestBrandsError });
    }
};

export const requestModels = () => async ({ dispatch, getState }) => {
    dispatch({ type: requestModelsLoading });
    const {
        form: { brand }
    } = getState();
    try {
        const request = await axios.get(`${baseUrl}models/${brand}`);
        const response = await request.data;
        dispatch({ type: requestModelsSuccess, payload: response.models });
    } catch (e) {
        dispatch({ type: requestModelsError });
    }
};
