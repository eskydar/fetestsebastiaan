export const onFieldChange = "onFieldChange";
export const clearField = "clearFields";

export const onChange = e => ({ dispatch }) => {
    const { name, value } = e.target;
    if (!name) throw new Error("Form elements require a name prop");
    dispatch({ type: onFieldChange, payload: { [name]: value } });
};
