import React from "react";
import styled from "styled-components";

const Btn = styled.button`
    --btn-bg: ${({ disabled }) =>
        disabled ? "var(--button-color-disabled)" : "var(--button-color)"};
    --btn-border: ${({ disabled }) =>
        disabled ? "1px solid var(--button-color)" : "none"};

    background: var(--btn-bg);
    width: 100%;
    padding: 13px;
    border-radius: 6px;
    cursor: pointer;
    font-weight: bold;

    border: var(--btn-border);
`;

const Button = ({ children, ...props }) => <Btn {...props}>{children}</Btn>;

export default Button;
