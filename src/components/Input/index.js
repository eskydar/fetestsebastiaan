import React from "react";
import styled from "styled-components";

const Inpt = styled.input`
    background: var(--select-background);
    border: 1px solid var(--border-color);
    padding: 10px;
    border-radius: 6px;
    width: 100%;
    font-size: 16px;
`;

const Input = props => <Inpt {...props} />;

export default Input;
