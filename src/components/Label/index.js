import React from "react";
import styled from "styled-components";

const Lbl = styled.label`
    font-weight: bold;
    margin-bottom: 8px;
    display: inline-block;
`;

const Label = props => <Lbl {...props} />;

export default Label;
