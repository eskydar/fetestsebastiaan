import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`;

export const Row = styled.div`
    margin-bottom: 15px;
    padding: 0px 25px;

    &:first-child {
        padding-top: 25px;
    }

    &:last-child {
        padding-bottom: 25px;
    }
`;

export const FormContainer = styled.form`
    border-bottom: 1px solid var(--border-color);
`;

export const ButtonContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 15px 25px;
`;
