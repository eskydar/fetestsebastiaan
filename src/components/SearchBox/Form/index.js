import React, { useEffect } from "react";

import { Container, Row, FormContainer, ButtonContainer } from "./Styles";
import {
    resetModels,
    requestBrands,
    requestModels
} from "../../../actions/carsActions";
import { onChange } from "../../../actions/formActions";
import useMapDispatch from "../../../hooks/useMapDispatch";
import useMapState from "../../../hooks/useMapState";
import Select from "../../Select";
import Input from "../../Input";
import Label from "../../Label";
import Button from "../../Button";

const Form = () => {
    const mapState = ({
        cars: { brands, models },
        form: { brand, model, keywords }
    }) => ({
        brands,
        models,
        brand,
        model,
        keywords
    });
    const state = useMapState(mapState);
    const actions = useMapDispatch(dispatch => ({
        getBrands: () => dispatch(requestBrands()),
        onChange: e => {
            e.persist && e.persist();
            dispatch(onChange(e));
        },
        clearModels: () => dispatch(resetModels()),
        getModels: () => dispatch(requestModels())
    }));

    useEffect(() => {
        actions.getBrands();
    }, []);

    useEffect(() => {
        if (state.brand) {
            actions.getModels();
        } else if (state.models.length) {
            actions.clearModels();
        }
    }, [state.brand]);

    const formIsValid = !(state.brand || state.model || state.keywords);

    return (
        <Container>
            <FormContainer>
                <Row>
                    <Label htmlFor="S1">Brand:</Label>
                    <Select
                        id="S1"
                        placeholder="- All Brands -"
                        options={state.brands.map(brand => ({
                            label: brand.name,
                            value: brand.id
                        }))}
                        name="brand"
                        onChange={actions.onChange}
                    />
                </Row>
                <Row>
                    <Label htmlFor="S2">Model:</Label>
                    <Select
                        id="S2"
                        placeholder="- All Models -"
                        options={state.models.map((model, i) => ({
                            label: model,
                            value: i
                        }))}
                        name="model"
                        onChange={actions.onChange}
                    />
                </Row>
                <Row>
                    <Label htmlFor="T">Keywords:</Label>
                    <Input id="T" name="keywords" onChange={actions.onChange} />
                </Row>
            </FormContainer>
            <ButtonContainer>
                <Button id="B" disabled={formIsValid}>
                    Search Cars
                </Button>
            </ButtonContainer>
        </Container>
    );
};

export default Form;
