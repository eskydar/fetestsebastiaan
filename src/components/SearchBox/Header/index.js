import React from "react";
import styled from "styled-components";

const Container = styled.div`
    padding: 25px;
    border-bottom: 1px solid var(--border-color);
`;
const H3 = styled.h3`
    color: var(--font-color);
`;

const Header = () => (
    <Container>
        <H3>Buy a car</H3>
    </Container>
);

export default Header;
