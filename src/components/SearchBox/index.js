import React from "react";
import styled from "styled-components";

import Header from "./Header";
import Form from "./Form";

const Container = styled.div`
    border: 1px solid var(--border-color);
    width: 356px;
    display: flex;
    flex-direction: column;

    margin: 20px auto;
`;

const SearchBox = () => (
    <Container>
        <Header />
        <Form />
    </Container>
);

export default SearchBox;
