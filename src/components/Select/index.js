import React from "react";
import styled from "styled-components";

const Container = styled.div`
    position: relative;
`;

const SelectInput = styled.select`
    background: var(--select-background);
    border: 1px solid var(--border-color);
    width: 100%;
    cursor: pointer;
    height: 40px;
`;

const Select = ({ options, placeholder, ...props }) => {
    const disabled = !options.length;
    return (
        <Container>
            <SelectInput {...props} disabled={disabled}>
                <option defaultValue value="">
                    {placeholder}
                </option>
                {options &&
                    options.map(({ label, value }, index) => (
                        <option key={index} value={value}>
                            {label}
                        </option>
                    ))}
            </SelectInput>
        </Container>
    );
};

export default Select;
