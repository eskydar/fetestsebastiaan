import { useContext } from "react";
import Context from "../../store/Context";

const useMapDispatch = mapDispatchFunc => {
    const { dispatch } = useContext(Context);
    return mapDispatchFunc(dispatch);
};

export default useMapDispatch;
