import { useContext } from "react";
import Context from "../../store/Context";

const useMapState = mapStateFunc => {
    const { store } = useContext(Context);
    return mapStateFunc(store);
};

export default useMapState;
