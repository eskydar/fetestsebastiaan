import {
    requestBrandsError,
    requestBrandsLoading,
    requestBrandsSuccess,
    requestModelsError,
    requestModelsLoading,
    requestModelsSuccess,
    clearModels
} from "../actions/carsActions";

const initialState = {
    brands: [],
    models: [],
    loading: false,
    error: false
};

const carsReducer = (state = initialState, action) => {
    switch (action.type) {
        case requestBrandsError:
            return { ...state, error: true, loading: false };
        case requestBrandsLoading:
            return { ...state, loading: true, error: false };
        case requestBrandsSuccess:
            return {
                ...state,
                loading: false,
                error: false,
                brands: action.payload
            };

        case requestModelsError:
            return { ...state, error: true, loading: false };
        case requestModelsLoading:
            return { ...state, loading: true, error: false };
        case requestModelsSuccess:
            return {
                ...state,
                loading: false,
                error: false,
                models: action.payload
            };

        case clearModels:
            return { ...state, models: initialState.models };
        default:
            return state;
    }
};

export default carsReducer;
