import { onFieldChange, clearField } from "../actions/formActions";

const initialState = {};

const carsReducer = (state = initialState, action) => {
    switch (action.type) {
        case onFieldChange:
            return { ...state, ...action.payload };
        case clearField:
            return { ...state, [action.payload]: null };
        default:
            return state;
    }
};

export default carsReducer;
