import combineReducers from "../store/combineReducers";

import cars from "./carsReducer";
import form from "./formReducer";

const reducer = combineReducers({
    cars,
    form
});

export default reducer;
