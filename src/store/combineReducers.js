let id = 0;
const combineReducers = reducer => {
    const withDevtools =
        typeof window !== "undefined" &&
        typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined";
    const reduxDevTools = withDevtools
        ? window.__REDUX_DEVTOOLS_EXTENSION__
        : null;
    const instanceID = id;
    id += 1;
    const name = `FeTestSebastiaan - ${instanceID}`;
    const devTools = withDevtools && reduxDevTools.connect({ name });
    return (state = {}, action) => {
        const keys = Object.keys(reducer);
        const nextReducers = {};
        for (let i = 0; i < keys.length; i++) {
            const invoke = reducer[keys[i]](state[keys[i]], action);
            nextReducers[keys[i]] = invoke;
        }
        withDevtools && devTools.send(action, nextReducers, {}, instanceID);
        return nextReducers;
    };
};

export default combineReducers;
